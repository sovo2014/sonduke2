var express = require('express');
var Repository = require(__dirname + '/models/repository.js');
var webSiteSchema = require(__dirname + '/models/webSiteSchema.js');
var repository = new Repository('mongodb://localhost/Sonduke2', webSiteSchema);

var app = express();
app.use(express.static(__dirname + '/app'));

function generalResponse(response){
  return function(err, data){
    response.send({err:err, data:data});
  };
}

app.get('/timestamp/:collectionName', function(req, res){
  repository.getCollectionTimestamp(req.params.collectionName, generalResponse(res));
});

app.get('/documents/:collectionName', function(req, res){
  repository.getCollectionDocuments(req.params.collectionName, generalResponse(res));
});

app.get('/document/:collectionName/:docId', function(req, res){
  repository.getCollectionDocumentById(req.params.collectionName, req.params.docId, generalResponse(res));
});

app.get('/document/:collectionName/new', function(req, res){
  repository.createCollectionDocument(req.params.collectionName, generalResponse(res));
});

//delete and all that jazz

app.listen(80);

