var Mongoose = require('mongoose').Mongoose;

function Repository(connectionString, getModelFunction){
  var mongoose = new Mongoose();
  this.db = mongoose.createConnection(connectionString);
  this.models = getModelFunction(this.db);
}

Repository.prototype.tryGetModel = function(collectionName, errorCallback, successCallback){
  var model = this.models[collectionName];
  if(!model)
    return errorCallback('no data exists');
  successCallback(model);
};

Repository.prototype.getCollectionDocuments = function(collectionName, callback){
  function success(model){
    model.find().setOptions({ lean: true }).exec(callback); 
  }
  this.tryGetModel(collectionName, callback, success);
};

Repository.prototype.getCollectionDocumentById = function(collectionName, docId, callback){
  function success(model){
    model.findById(docId).setOptions({ lean: true }).exec(callback);  
  }
  this.tryGetModel(collectionName, callback, success);
};

Repository.prototype.createCollectionDocument = function(collectionName, callback){
  var self = this;
  function success(model){
    var name = 'new ' + collectionName;
    function updateTimestampAndReturn(err, document){
      if(err)
        return callback(err);
      self.updateCollectionTimestamp(collectionName, function onTimestampUpdated(err){
        callback(err, document);
      });  
    }
    model.create({name:name, description:'Description of '+name}, updateTimestampAndReturn);
  }
  this.tryGetModel(collectionName, callback, success);
};

Repository.prototype.getCollectionTimestamp=function(collectionName, callback){
  var self = this;
  function success(model){
    model.findOne({name:collectionName}).setOptions({lean:true}).exec(function(err, timestamp){
      if(err)
        return callback(err);
      if(!timestamp)  
        return self.updateCollectionTimestamp(collectionName, callback);
      return callback(err, timestamp);  
    });
  }
  this.tryGetModel('timestamps', callback, success);
};

Repository.prototype.updateCollectionTimestamp=function(collectionName, callback){
  function success(model){
    var now = (new Date()).getTime();
    model.findOneAndUpdate({name:collectionName}, {timestamp:now}, {upsert:true}, callback);
  }
  this.tryGetModel('timestamps', callback, success);
};

module.exports = Repository;