var Mongoose = require('mongoose').Mongoose;
var Schema = require('mongoose').Schema;

var CountrySchema = new Schema({
  Country:String,
  Capital:String,
  MapReference:String,
  Currency:String,
  Population:Number
});

var mapReferences = [ 'Asia',
  'Europe',
  'Africa',
  'Oceania',
  'Central America and the Caribbean',
  'Antarctic Region',
  'South America',
  'Commonwealth of Independent States',
  'Southeast Asia',
  'Middle East',
  'North America'
  ];

var Repository = function(){
  var mongoose = new Mongoose();
  var connection_string = 'mongodb://localhost/SharedEntities';
  this.db = mongoose.createConnection(connection_string);  
  this.Country = this.db.model('Countries', CountrySchema, 'Countries');
};

Repository.prototype.fixMapReferences = function(callback){
  var self = this;
  function fixReference(refs, index, next){
    if(index < refs.length){
      var ref = refs[index];
      self.Country.update({'MapReference':ref}, {'MapReference':ref.trim()}, function(err){
        if(err)
          return callback(err);
        fixReference(refs, index+1, next);  
      });
    }
    else{
      next();
    }
  }

  this.getMapReferences(function(err, refs){
    if(err)
      return callback(err);
    fixReference(refs, 0, callback);  
  });
};

Repository.prototype.getCountry = function(country, callback){
  this.Country.findOne({Country:country}).setOptions({ lean: true }).exec(function(err, c){
    if(err) return callback(err);
    c.MapReference = c.MapReference.trim();
    callback(null, c);
  });
};

Repository.prototype.getCountries = function(mapReference, callback){
  function onData(err, data){
    if(err) return callback(err);
    data.forEach(function(dataItem){
      dataItem.MapReference = dataItem.MapReference.trim();
    });
    callback(null, data);
  }
  this.Country.find({MapReference:{$in: [mapReference, mapReference+' ']}, Country:{$exists : true}}).setOptions({ lean: true }).exec(onData);
};

Repository.prototype.getMapReferences = function(callback){
  callback(null, mapReferences);
};

module.exports = Repository;