var Schema = require('mongoose').Schema;

var ApplicationSchema = new Schema({
  name: {type: String, default:''},
  description: {type:String, default:''}
});

var CollectionTimestampSchema = new Schema({
  name: String,
  timestamp: Number
});

var SidebarSchema = new Schema({
  name: String,
  id: String
});

module.exports = function getModels(db){
  var applications = db.model('Application', ApplicationSchema);  
  var timestamps = db.model('CollectionTimestamp', CollectionTimestampSchema);
  var sidebar = db.model('WebSiteSidebar', SidebarSchema, 'websitesidebar');
  return {
    'applications': applications,
    'timestamps': timestamps,
    'sidebar':sidebar
  };
};
