'use strict';

/* Sonduke web-site Module */

angular.module('webSiteApplication', ['dataProviderModule', 'components']).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/:navItemId', {}).
      when('/:navItemId/:itemId',{}).
      when('/', {})
      .otherwise({redirectTo: '/applications'});
}]);

angular.module('dataProviderModule', [], function($provide){
  $provide.factory('dataConfig', function(){
  
    amplify.request.define('createDocument', 'ajax',
      {
        url: '/document/{collectionId}/new',
        type: 'GET',
        'dataType':'json'
      }
    );    
    amplify.request.define('documents', 'ajax',
      {
        url: '/documents/{collectionId}',
        type: 'GET',
        dataType:'json',
        cache: 'timestampCache'
      }
    );
    amplify.request.define('document', 'ajax',
      {
        url: '/document/{collectionId}/{documentId}',
        type: 'GET',
        'dataType':'json'
      }
    );
    amplify.request.define('timestamp', 'ajax',
      {
        url: 'timestamp/{collectionId}',
        type: 'GET',
        'dataType':'json'
      }
    );
    var dataProvider = new DataProvider();
    dataProvider.appTitle = 'sonduke';
    dataProvider.getTemplateUrl = function(templateId){
      if(templateId=='sidebar')
        return '/webSiteTemplates/sidebar.html';
      if(templateId.indexOf('_item')>0)  
        return '/webSiteTemplates/application.html';
      return '/webSiteTemplates/applications.html';  
    };
    return dataProvider; 
  });
});
