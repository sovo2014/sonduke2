
var DataProvider = function(){
  this.timestamps={};
  this.useCache = true;
  var self = this;
  amplify.request.cache.timestampCache = cache;
  function cache(resource, settings, ajx, ampXHR) {
    var cacheKey = amplify.request.cache._key(settings.resourceId, ajx.url, ajx.data);
    var cached = amplify.store(cacheKey);
    if(cached && self.useCache) {
      ajx.success(cached);
      return false;
    }
    var success = ampXHR.success;
    ampXHR.success = function(data) {
      amplify.store(cacheKey, data);
      self.useCache = true;
      success.apply(this, arguments);
    };
  };
};

DataProvider.prototype.parseResourceId = function(resourceId){
  var resourceIdParts = resourceId.split(':');
  var requestParams = {};
  if(resourceIdParts.length > 1)
    requestParams.collectionId = resourceIdParts[1];
  if(resourceIdParts.length > 2)
    requestParams.documentId = resourceIdParts[2];   
  return {
    resourceId:  resourceIdParts[0],
    data: requestParams
  };  
};

DataProvider.prototype.getResource = function(resourceId, callback){
  var localTimestamp = this.timestamps[resourceId];
  var requestParams = this.parseResourceId(resourceId);
  requestParams.success = dataReceived;
  amplify.request({
    resourceId: 'timestamp',
    data: requestParams.data,
    success: timestampReceived
  });   
  var self = this;
  var serverTimestamp;
  function dataReceived(response){
    self.timestamps[resourceId] = serverTimestamp;
    callback(response);  
  }
  function timestampReceived(response){
    if(response.err)
      return callback(response.err);
    var data = response.data;  
    self.useCache = data.timestamp == localTimestamp;
    serverTimestamp = data.timestamp;
    amplify.request(requestParams);
  }
};