'use strict';

var dpModule = angular.module('dataProviderModule', []);

dpModule.factory('dataConfig', function() {

    amplify.request.define('sidebar', 'ajax',
    {
      url: '/mapReferences',
      type: 'GET',
      dataType: 'json'
    });   
    amplify.request.define('list', 'ajax',
    {
      url: '/countries/{listId}',
      type: 'GET',
      'dataType':'json'
    });
    amplify.request.define('details', 'ajax',
    {
      url: '/country/{detailsId}',
      type: 'GET',
      'dataType':'json'
    });
    
    return {
      appTitle:'World map',
      getResource: function(resource, callback){
        
      },
      getTemplateUrl: function(templateId){
        if(templateId=='sidebar')
          return '/demo_templates/sidebar.html';
        if(templateId.indexOf('_item')>0)  
          return '/demo_templates/country.html';
        return '/demo_templates/countries.html';  
      }
    }
});