'use strict';



function ApplicationController($scope, $route, $location, dataConfig){
  function initializeCommand(command){
    $scope.$watch(command.enabledStateTrigger, function(newValue, oldValue){
      if(newValue!=oldValue)
      command.updateEnabledState(newValue);
    });
  }
  
  var commandNew = {
    title: 'New...',
    enabled: true,
    icon:'icon-file',
    updateEnabledState: function(itemId){
      this.enabled = true;
    },
    execute: function(){
      $location.path("/applications/new");
    },
    enabledStateTrigger: 'itemId'
  };    

  $scope.initialized = false;
  $scope.sidebar = {
    watchRequired:false
  };
  $scope.list = {
    commands:[commandNew],
    watchRequired:true
  };
  $scope.list.commands.forEach(function(command){
    initializeCommand(command);
  });

  $scope.details = {
    watchRequired:false
  };
  
  function initialize(routeParams){
    if(!$scope.initialized)
      $scope.initialized = true;
    $scope.appTitle = dataConfig.appTitle;  
    $scope.sidebarItemSelected = routeParams.navItemId && routeParams.navItemId.length;
    $scope.detailsItemSelected = routeParams.itemId && routeParams.itemId.length;
    
    var sidebar = $scope.sidebar;
    sidebar.template = 'sidebar';
    sidebar.dataResourceId = 'sidebar';
    sidebar.selectedItemId = routeParams.navItemId;
    if(!$scope.sidebarItemSelected)
      return;
      
    var list = $scope.list;    
    list.selectedItemId= routeParams.itemId;
    list.template = routeParams.navItemId;
    list.dataResourceId= 'list:'+routeParams.navItemId;

    if(!$scope.detailsItemSelected){
      return;
    }
      
    var details = $scope.details;  
    details.detailsId = routeParams.itemId;
    details.template = routeParams.navItemId+'_item';
    details.dataResourceId = 'details:'+routeParams.navItemId+':'+routeParams.itemId;
  }
  $scope.$on('$routeChangeStart', function (scope, next, current) {
    initialize(next.params);
  });  
}