var componentsModule = angular.module('components', []);


componentsModule.directive('dataview', function(dataConfig){
  return {
    restrict: 'E',
    replace: true,
    scope:{
      'resourceId': '=',
      'templateId': '=',
      'selectedItemId':'=',
      'detailsId':'=',
      'watchRequired':'='
    }, 
    template:'<ng-include src="templateUrl"></ng-include>',
    controller: function($scope, $element, $attrs, dataConfig, $location){
      function setTemplate(templateId){
        $scope.templateUrl = dataConfig.getTemplateUrl(templateId);
      }
      var workerId;
      function setData(resourceId){
        if(!$scope.watchRequired)
          return setDataInternal(resourceId);
        if(workerId)
          clearInterval(workerId);
        workerId = setInterval(function(){setDataInternal(resourceId)}, 3000);  
      }
      function setDataInternal(resourceId){
        dataConfig.getResource(resourceId, function(data){
          if(data.err)
            return;
          $scope.$apply(function(){
            $scope.data = data.data;
          });            
        });
      }
      setTemplate($scope.templateId);
      setData($scope.resourceId);
      $scope.$watch('templateId', function(newValue, oldValue){
        if(newValue!=oldValue)
          setTemplate(newValue);
      });
      $scope.$watch('resourceId', function(newValue, oldValue){
        if(newValue!=oldValue)
          setData(newValue);
      });
    }
  };
});

componentsModule.directive('hrefNoTouch', function($location){
  return function(scope, element, attr) {
    element.click(function(e) {
      location.href = attr.hrefNoTouch;
      //$location.path(attr.hrefNoTouch);
      return false;
    });    
  };
});

componentsModule.directive('uiIf', [function () {
  return {
    transclude: 'element',
    priority: 1000,
    terminal: true,
    restrict: 'A',
    compile: function (element, attr, linker) {
      return function (scope, iterStartElement, attr) {
        iterStartElement[0].doNotMove = true;
        var expression = attr.uiIf;
        var lastElement;
        var lastScope;
        scope.$watch(expression, function (newValue) {
          if (lastElement) {
            lastElement.remove();
            lastElement = null;
          }
          if (lastScope) {
            lastScope.$destroy();
            lastScope = null;
          }
          if (newValue) {
            lastScope = scope.$new();
            linker(lastScope, function (clone) {
              lastElement = clone;
              iterStartElement.after(clone);
            });
          }
          iterStartElement.parent().trigger("$childrenChanged");
        });
      };
    }
  };
}]);    